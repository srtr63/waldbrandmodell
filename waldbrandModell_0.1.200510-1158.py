#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Sven Reiter"
__version__ = "0.1.200510-1158"
"""
Dem Benutzer wird freigestellt, ob er das Programm auf Basis der waldbrand.inp startet oder
 ob ein bestehendes/r aber nicht abgeschlossenes/r Projekt/Simulationsverlauf fortgefueht wird.

Wenn das Programm beendet wird und es wurde beim Start ein Projekt angegeben, 
 wird die .out-Datei nicht beschrieben
 Viel mehr wird die .out nur vollstaendig beschrieben wenn das Projekt erstellt wird

ab l190
 die existierende.out wird unveraendert eingelesen und 
 mylines ist so beschrieben, dass der Inhalt nur noch bei "'\n'','' '" gesplitet werden muss und
 mit der daraus resultierenden 2-dimensionale Sequenz anschließend <treeDict> definiert werden kann.
 
Die eingelesenen Werte aus <treeDicts> sehen vielversprechend aus

Nachdem ein geladenes Projekt geladen wurde und es weiter gearbeitet hat, 
 die Simulation aber wieder das Simulationsende nicht erreicht hat und vorher abgebrochen wurde,
 wird die existierende .out gelöscht und eine neue .out erzeugt/beschrieben

Man kann Projekte speichern/laden bzw. Simulationslaeufe unterbrechen und entspr. Lauf fortfuehren,
indem man den Namen des Projekts als Argument angibt.

Jeder Restart eines Projekts wird in einem seperaten Verzeichnis abgespeichert.

"""

import numpy as np
import matplotlib.pyplot as plt
import os, cv2, time, csv, json, errno,sys,glob
from matplotlib import animation
from matplotlib import colors
from pathlib import Path
from shutil import copyfile
from celluloid import Camera

class WaldbrandModell:
    timeSeed=0
    FPS = 10
    VERSION=""
    GRIDWIDTH = 640
    GRIDHEIGHT = 480
    CELLSIZE = 10
    BLACK =    (0,  0,  0)
    WHITE =    (255/255,255/255,255/255)    #ValueError: RGBA values should be within 0-1 range
    DARKGRAY = (40/255, 40/255, 40/255)
    GREEN =    (0,255/255,0)
    RED =      (255/255,0,0)
    GAMMA = 0.00005
    ALPHA = 0.01
    ASCHE=0
    BAUM=1
    FEUER=2
    END_TICK=(-1)
    START_TICK=0
    cmap = colors.ListedColormap([DARKGRAY, GREEN, RED])  #color() braucht es nicht wegen Normalisierung, Wert-Farbe per colormap 
    bounds = [0,1,2,3]                                    #   Norm:       zwischen 0-1:=1.Farbe,zwischen 1-2:=2.Farbe,zwischen 2-3:=3.Farbe; 
    norm = colors.BoundaryNorm(bounds, cmap.N)            #Normalisierung::   Wert:Color,bounds: die Bereiche,cmap.N: Anzahl der Teilbereiche
    
    treeDict  = np.zeros((GRIDHEIGHT, GRIDWIDTH))
    simFig = plt.figure()       # <- das funktioniert es benutzt den DEFAULT-Wert
    
    bck = plt.get_backend()
    mng = plt.get_current_fig_manager()
    if (bck == "TkAgg"):   
        mng.window.resizable(False, False)
    
    ax = simFig.add_subplot(111)   # 1row 1colm 1.subplot
    ax.set_axis_off()           # um die Achsenbeschriftung auszuschalten
    im = ax.imshow(treeDict, cmap=cmap, norm=norm)#, interpolation='nearest')   # displaying data as image
                                                                                # X: ArrayImageValsForColormap
                                                                                # cmap: Colormap
    def closeOp(self,msg):
        global tickNr,treeDict,existingFile
        #Hier werden alle Diagramme gezeichnet, das Video mit cvs geschnitten ... und und und ...
        baums=[]
        asches=[]
        feuers=[]
        
        with open(allCsvPath, "r") as csvdatei:
            csv_reader_object = csv.DictReader(csvdatei)
            for row in csv_reader_object:           # csv_reader_object wird iteriert als <row>
                for key, value in row.items():      # <row.items> wird separiert in key- und value- Teil
                    if key=="AmtTree":
                        baums.append(int(row["AmtTree"]))
                    elif key=="AmtAsh":
                        asches.append(int(row["AmtAsh"]))
                    elif key=="AmtFire":
                        feuers.append(int(row["AmtFire"]))
        
        figure =plt.figure()
        allPlot=figure.add_subplot(1,1,1)
        allPlot.set_xlabel('Simulation progress[Tick]')
        allPlot.set_ylabel('Number cells(red:=fire,green:=tree,gray:=ash)')
        allPlot.plot(baums,color='green')
        allPlot.plot(asches,color='darkgray')
        allPlot.plot(feuers,color='red')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/all_'+berechnungslaufkennung+'.png')
        plt.close(figure)
        
        figure =plt.figure()
        baumPlot=figure.add_subplot(1,1,1)
        baumPlot.set_xlabel('Simulation progress[Tick]')
        baumPlot.set_ylabel('Number Tree-cells')
        baumPlot.plot(baums,color='green')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/tree_'+berechnungslaufkennung+'.png')
        plt.close(figure)
        
        figure =plt.figure()
        aschePlot=figure.add_subplot(1,1,1)
        aschePlot.set_xlabel('Simulation progress[Tick]')
        aschePlot.set_ylabel('Number Ash-cells')
        aschePlot.plot(asches,color='darkgray')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/ash_'+berechnungslaufkennung+'.png')
        plt.close(figure)
        
        figure =plt.figure()
        feuerPlot=figure.add_subplot(1,1,1)
        feuerPlot.set_xlabel('Simulation progress[Tick]')
        feuerPlot.set_ylabel('Number Fire-cells')
        feuerPlot.plot(feuers,color='red')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/fire_'+berechnungslaufkennung+'.png')
        plt.close(figure)
            
        with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".out","a") as ausgabedateiOut:
            ausgabedateiOut.write("\nCalculation results:\t/waldbrand_run_"+berechnungslaufkennung+"/timestemps/step_"+berechnungslaufkennung+"_%05d.png ... step_"%(self.START_TICK)+berechnungslaufkennung+"_%05d"%(tickNr)+".png\n")
#            ausgabedateiOut.write("\t\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/movies/steps_"+berechnungslaufkennung+"_%05d"%(self.START_TICK)+"-%05d"%(tickNr)+".mp4\n")
            ausgabedateiOut.write("\t\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/graphs/tree_"+berechnungslaufkennung+".png\n")
            ausgabedateiOut.write("\t\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/graphs/ash_"+berechnungslaufkennung+".png\n")
            ausgabedateiOut.write("\t\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/graphs/fire_"+berechnungslaufkennung+".png\n")
            ausgabedateiOut.write("\t\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/graphs/all_"+berechnungslaufkennung+".png\n")
            ausgabedateiOut.write("Tables:\t\t\t\t\t/waldbrand_run_"+berechnungslaufkennung+"/graphs/all_"+berechnungslaufkennung+".csv\n")
            
            if msg=="Simulation aborded":
                ausgabedateiOut.write("Simulation aborded after step %d\n"%(tickNr-1))
                for rows in range(self.GRIDHEIGHT):
                    for cells in range(self.GRIDWIDTH):
                        ausgabedateiOut.write(str(int(self.treeDict[rows][cells])))
                        if((cells+1)<self.GRIDWIDTH):
                            ausgabedateiOut.write(',')
                    ausgabedateiOut.write("\n")
                
            ausgabedateiOut.close                   
        videoFile = "waldbrand_run_"+berechnungslaufkennung+'/movies/steps_'+berechnungslaufkennung+'_'+str("%05d"%(self.START_TICK)+'-'+str("%05d"%(tickNr)+'.mp4'))
        filenames = []
        for filename in glob.glob(str(timestepsPath)+'/*.png'):
            filenames.append(filename)

        filenames.sort()

        img_array = []
        size = (self.GRIDWIDTH,self.GRIDHEIGHT)
        for filename in filenames:
            img = cv2.imread(filename)
            height,width,layers = img.shape
            img_array.append(img)

        out = cv2.VideoWriter(videoFile,cv2.VideoWriter_fourcc(*'mp4v'), 10, size)

        for i in range(len(img_array)):
            out.write(img_array[i])
        out.release()
        out = msg+"\t"+time.strftime("%y%m%d-%H%M%S", time.localtime())
        with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
            logdateiOut.write(out+"\n")
            logdateiOut.close
        
        print(out)

    def handle_close(self,evt):
        self.closeOp("Simulation aborded")
    
    def __init__(self):
        global eingabedatei,existingFile,berechnungslaufkennung,eingabedateiPath,ausgabedateiPath,timestepsPath,ascheGraphPath,baumGraphPath,feuerGraphPath,allGraphPath,allCsvPath,allCsvOut,tickNr,START_JOB
        self.simFig.canvas.mpl_connect('close_event',self.handle_close)
        existingFile=False
        
        if len(sys.argv)>1:
            #Pruefen ob angegebenes Projekt existiert
            if not Path(sys.argv[1]).is_dir():
                print("\tProject not available!\n")
                sys.exit(1)
            else:
                existingFile=True
            
            #Die projektspezifische <berechnungslaufkennung> muss extrahiert werden, um auf seine .out referenzieren zu koennen.
            berechnungslaufkennungOrig = sys.argv[1][14:27]
            
            #Eine neue/aktuelle <berechnungslaufkennung> muss fuer den Restart generiert werden.
            named_tuple = time.localtime()
            berechnungslaufkennung = time.strftime("%y%m%d-%H%M%S", named_tuple)
            
            #neue Projektstruktur/Ordnerstruktur
            try:
                os.mkdir("waldbrand_run_"+berechnungslaufkennung)
            except OSError as exc:
                if exc.errno==errno.EEXIST:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
                elif exc.errno==errno.EACCES:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                elif exc.errno==errno.EROFS:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
                elif exc.errno==errno.ENOENT:
                   print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
                exit(1)
                
            timestepsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/timesteps")
            graphsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/graphs")
            moviesPath=Path("waldbrand_run_"+berechnungslaufkennung+"/movies")
            try:
                os.mkdir(timestepsPath)
                os.mkdir(graphsPath)
                os.mkdir(moviesPath)
            except OSError as exc:
                if exc.errno==errno.EEXIST:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
                        logdateiOut.close
                elif exc.errno==errno.EACCES:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                        logdateiOut.close
                elif exc.errno==errno.EROFS:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
                        logdateiOut.close
                elif exc.errno==errno.ENOENT:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
                        logdateiOut.close
                exit(1)
            #log-Datei erzeugen
            logdateiPath=Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log")
            logdateiOut=open(logdateiPath,"a")
            allCsvPath=Path(str(graphsPath)+"/all_"+berechnungslaufkennung+".csv")
            
            try:
                allCsvOut=open(allCsvPath,"a")
            except FileNotFoundError:
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write("When opening "+graphsPath+": No such directory entry\n")
                logdateiOut.close
            
            allCsvOut.write("Timestep,AmtTree,AmtAsh,AmtFire\n")
            
            readIt=False
            mylines=[]
            row=i=0
            for line in open("waldbrand_run_"+berechnungslaufkennungOrig+"/waldbrand_"+berechnungslaufkennungOrig+".out",'r'):
                if "Simulation aborded after step " in line:
                    tickNr = (int)(line[30:len(line)])
                    tickNr+=1
#                    print("Der eingelesene Simulationsschritt bei dem die Simulation abgebrochen wurde ist "+str(tickNr))
                    readIt=True
                if readIt and "Simulation aborded after step " not in line:
                    mylines.append(line)
                    for val in line.split(','):
                        #print(val)
                        if val!='\n' and val!=',' and val!='':
                            self.treeDict[row][i]=int(val)
                        i+=1
                    i=0
                    row+=1
            
            v=w=h=g=a=s=False
            jsondatei = json.load(open("waldbrand.inp"))
            for key in jsondatei:
                if key=="VERSION":
                    self.VERSION=str(jsondatei[key])
                    v=self.VERSION!=""
#                    print(" In geladenem <VERSION> steht: "+self.VERSION)  #TEST
                elif key=="GRIDWIDTH":
                    self.GRIDWIDTH=int(jsondatei[key])
                    w=self.GRIDWIDTH!=0
#                    print(" In geladenem <WIDTH> steht: "+str(self.GRIDWIDTH))  #TEST
                elif key=="GRIDHEIGHT":
                    self.GRIDHEIGHT=int(jsondatei[key])
                    h=self.GRIDHEIGHT!=0
#                    print(" In geladenem <HEIGHT> steht: "+str(self.GRIDHEIGHT)) #TEST
                elif key=="GAMMA":
                    self.GAMMA = float(jsondatei[key])
                    g=self.GAMMA!=0.0
#                    print(" In geladenem <GAMMA> steht: "+str(self.GAMMA))  #TEST
                elif key=="ALPHA":
                    self.ALPHA = float(jsondatei[key])
                    a=self.ALPHA!=0.0
#                    print(" In geladenem <ALPHA> steht: "+str(self.ALPHA))  #TEST
                elif key=="SEED":
                    self.timeSeed=int(jsondatei[key])
                    s=self.timeSeed!=0
                    self.timeSeed&=0xffffff
#                    print(" In geladenem <SEED> steht: "+str(self.timeSeed))   #TEST
                elif key=="END_TICK":
                    self.END_TICK = int(jsondatei[key])
#                    print(" In geladenem <END_TICK> steht: "+str(self.END_TICK))   #TEST
                elif key=="START_TICK":
                    self.START_TICK = int(jsondatei[key])
#                    print(" In geladenem <START_TICK> steht: "+str(self.START_TICK))   #TEST
                else:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write(eingabedatei+" contains the redundant information "+key+"\n")
                        logdateiOut.close
            
            if v==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about Version\n")
                    logdateiOut.close
            if w==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about vertical cell amount\n")
                    logdateiOut.close
            if h==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about horizontal cell amount\n")
            if g==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about probability Gamma\n")
                    logdateiOut.close
            if a==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about probability Alpha\n")
                    logdateiOut.close
            if s==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about Seed\n")
                    logdateiOut.close
            if v==w==h==g==a==s==v==True: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write("The information from "+'waldbrand_run_'+berechnungslaufkennung+'/waldbrand_'+berechnungslaufkennung+".inp is complete\n")
                    logdateiOut.close
            
            self.anim = animation.FuncAnimation(self.simFig, self.animation, interval=self.FPS)
            
            if(self.timeSeed == 0):
                self.timeSeed = int(round(time.time()))
                self.timeSeed&=0xffffff #seed() fordert einen vorzeichenlosen 32bit Integer, deswegen die verUNDung 
                                        #so dass existierendes Muster erhalten bleibt und Vorzeichenbit 0 wird.
            np.random.seed(self.timeSeed)
            
#           TODO
            if self.START_TICK>0:
                print("Loading/calculating the required simulationstep\n...")
                while(tickNr<(self.START_TICK)):    # DER FEHLER: while(tickNr<=(self.START_TICK)):
                    self.treeDict = self.tick(self.treeDict)       #tick bzw. aktuallisierung jeder informativen Zelle
                    tickNr+=1
            
            try:    
                copyfile("waldbrand.inp",'waldbrand_run_'+berechnungslaufkennung+'/waldbrand_'+berechnungslaufkennung+".inp")
            except IOError as exc:     # zB. wenn die Berechtigung fehlt
                if exc.errno==errno.EACCES:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                        logdateiOut.close
                exit(1)
            
            
            if len(sys.argv)==1:
                if not existingFile:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".out","a") as ausgabedateiOut:
                        ausgabedateiOut.write("Program:\t\t\t\t/waldbrand.inp\nProgram version:\t\t"+self.VERSION+"\nInput data:\t\t\t\t/"+eingabedatei)
                        ausgabedateiOut.close
                    
            self.anim = animation.FuncAnimation(self.simFig, self.animation, interval=self.FPS)
                
        else:
            named_tuple = time.localtime()
            berechnungslaufkennung = time.strftime("%y%m%d-%H%M%S", named_tuple)
            
            #2. Verzeichnis "waldbrand_run_"+<berechnungslaufkennung> anlegen.
            try:
                os.mkdir("waldbrand_run_"+berechnungslaufkennung)
            except OSError as exc:
                if exc.errno==errno.EEXIST:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
                elif exc.errno==errno.EACCES:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                elif exc.errno==errno.EROFS:
                    print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
                elif exc.errno==errno.ENOENT:
                   print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
                exit(1)
                
            #4. weitere Ordnerstruktur arrangieren
            timestepsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/timesteps")
            graphsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/graphs")
            moviesPath=Path("waldbrand_run_"+berechnungslaufkennung+"/movies")
            
            try:
                os.mkdir(timestepsPath)
                os.mkdir(graphsPath)
                os.mkdir(moviesPath)
            except OSError as exc:
                if exc.errno==errno.EEXIST:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
                        logdateiOut.close
                elif exc.errno==errno.EACCES:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                        logdateiOut.close
                elif exc.errno==errno.EROFS:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
                        logdateiOut.close
                elif exc.errno==errno.ENOENT:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
                        logdateiOut.close
                exit(1)
            
            ascheGraphPath=Path(str(graphsPath)+"/ash_"+berechnungslaufkennung+".png")
            baumGraphPath=Path(str(graphsPath)+"/tree_"+berechnungslaufkennung+".png")
            feuerGraphPath=Path(str(graphsPath)+"/fire_"+berechnungslaufkennung+".png")
            allGraphPath=Path(str(graphsPath)+"/all_"+berechnungslaufkennung+".png")
            allCsvPath=Path(str(graphsPath)+"/all_"+berechnungslaufkennung+".csv")
            try:
                allCsvOut=open(allCsvPath,"a")
            except FileNotFoundError:
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write("When opening "+graphsPath+": No such directory entry\n")
                logdateiOut.close
            
            allCsvOut.write("Timestep,AmtTree,AmtAsh,AmtFire\n")
            
            
            logdateiPath=Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log")
            logdateiOut=open(logdateiPath,"a")
            
            eingabedatei="waldbrand.inp"
            if not Path(eingabedatei).exists():
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write("When opening "+str(graphsPath)+": No such File found\n")
                logdateiOut.close
                print("When opening "+str(eingabedatei)+": No such File found\n")
                return;
                
            print("Reading "+str(eingabedatei))
            
            v=w=h=g=a=s=False
            jsondatei = json.load(open(eingabedatei))
            for key in jsondatei:
                if key=="VERSION":
    #                            print(" In geladenem <VERSION> steht: "+row["Value"])  #TEST
                    self.VERSION=str(jsondatei[key])
                    v=self.VERSION!=""
                elif key=="GRIDWIDTH":
    #                            print(" In geladenem <WIDTH> steht: "+row["Value"])  #TEST
                    self.GRIDWIDTH=int(jsondatei[key])
                    w=self.GRIDWIDTH!=0
                elif key=="GRIDHEIGHT":
    #                            print(" In geladenem <HEIGHT> steht: "+row["Value"]) #TEST
                    self.GRIDHEIGHT=int(jsondatei[key])
                    h=self.GRIDHEIGHT!=0
                elif key=="GAMMA":
    #                            print(" In geladenem <GAMMA> steht: "+row["Value"])  #TEST
                    self.GAMMA = float(jsondatei[key])
                    g=self.GAMMA!=0.0
                elif key=="ALPHA":
    #                            print(" In geladenem <ALPHA> steht: "+row["Value"])  #TEST
                    self.ALPHA = float(jsondatei[key])
                    a=self.ALPHA!=0.0
                elif key=="SEED":
                    self.timeSeed=int(jsondatei[key])
                    s=self.timeSeed!=0
                    self.timeSeed&=0xffffff
    #                            print(" In geladenem <SEED> steht: "+row["Value"])   #TEST
                elif key=="END_TICK":
                    self.END_TICK = int(jsondatei[key])
                elif key=="START_TICK":
                    self.START_TICK = int(jsondatei[key])
                else:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write(eingabedatei+" contains the redundant information "+key+"\n")
                        logdateiOut.close
            
            if v==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about Version\n")
                    logdateiOut.close
            if w==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about vertical cell amount\n")
                    logdateiOut.close
            if h==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about horizontal cell amount\n")
            if g==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about probability Gamma\n")
                    logdateiOut.close
            if a==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about probability Alpha\n")
                    logdateiOut.close
            if s==False: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write(eingabedatei+" contains no information about Seed\n")
                    logdateiOut.close
            if v==w==h==g==a==s==v==True: 
                with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                    logdateiOut.write("The information from "+eingabedatei+" is complete\n")
                    logdateiOut.close
            
            try:    
                copyfile(eingabedatei,'waldbrand_run_'+berechnungslaufkennung+'/waldbrand_'+berechnungslaufkennung+".inp")
            except IOError as exc:     # zB. wenn die Berechtigung fehlt
                if exc.errno==errno.EACCES:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log",'a') as logdateiOut:
                        logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
                        logdateiOut.close
                exit(1)
            
            if len(sys.argv)==1:
                if not existingFile:
                    with open("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".out","a") as ausgabedateiOut:
                        ausgabedateiOut.write("Program:\t\t\t\t/"+eingabedatei+"\nProgram version:\t\t"+self.VERSION+"\nInput data:\t\t\t\t/"+eingabedatei)
                        ausgabedateiOut.close
                
            self.anim = animation.FuncAnimation(self.simFig, self.animation, interval=self.FPS)
            
            if(self.timeSeed == 0):
                self.timeSeed = int(round(time.time()))
                self.timeSeed&=0xffffff #seed() fordert einen vorzeichenlosen 32bit Integer, deswegen die verUNDung 
                                        #so dass existierendes Muster erhalten bleibt und Vorzeichenbit 0 wird.
            np.random.seed(self.timeSeed)
            
            tickNr=0
            if self.START_TICK>0:
                print("Loading/calculating the required simulationstep\n...")
                while(tickNr<(self.START_TICK)):    # DER FEHLER: while(tickNr<=(self.START_TICK)):
                    self.treeDict = self.tick(self.treeDict)       #tick bzw. aktuallisierung jeder informativen Zelle
                    tickNr+=1
       
        print("Simulation started!")
        self.START_JOB = time.time()
        plt.show()
        
    def isNeigborhoodBurning(self,ix,iy,treeDictArg):
        for y in range (-1,2):
            for x in range (-1,2):
                checkCell = (ix+x,iy+y)
                if checkCell[0] <  (self.GRIDHEIGHT) and checkCell[0] >=0:
                    if checkCell[1] < (self.GRIDWIDTH) and checkCell[1]>= 0:
                        if treeDictArg[checkCell] == self.FEUER:
                            return True
        return False
    
    def tick(self,treeDictArg):
        global allCsvOut,tickNr
        baumCnt=0
        ascheCnt=(self.GRIDHEIGHT*self.GRIDWIDTH)
        feuerCnt=0
        newTick = np.zeros((self.GRIDHEIGHT, self.GRIDWIDTH))
        
        if tickNr == self.START_TICK == 0:
            allCsvOut=open(allCsvPath,"a")
            allCsvOut.write("%7d, %7d, %7d, %7d\n" % (tickNr,baumCnt,ascheCnt,feuerCnt))
            allCsvOut.close()
            print("\tStep:%7d\tTree:%7d\tAsh:%7d\tFire:%7d" % (tickNr,baumCnt,ascheCnt,feuerCnt))
            #tickNr=1
            return newTick
        
        for iy in range(1,(self.GRIDHEIGHT-1)):
            for ix in range(1,(self.GRIDWIDTH-1)):
                
                if treeDictArg[iy,ix] == self.ASCHE and np.random.random() <= self.ALPHA:
                    newTick[iy,ix] = self.BAUM
                    baumCnt+=1
                    ascheCnt-=1
     
                elif treeDictArg[iy,ix] == self.BAUM:
                    if self.isNeigborhoodBurning(iy,ix,treeDictArg):
                        newTick[iy,ix] = self.FEUER
                        feuerCnt+=1
                        ascheCnt-=1
                    else:
                        if np.random.uniform(0.,1.) < self.GAMMA:
                            newTick[iy,ix] = self.FEUER
                            feuerCnt+=1
                            ascheCnt-=1
                        else:
                            newTick[iy,ix] = self.BAUM
                            baumCnt+=1
                            ascheCnt-=1
                elif treeDictArg[iy,ix] == self.FEUER:
                    newTick[iy,ix] = self.ASCHE
        
        if tickNr>=self.START_TICK:         # DIESE BEDINGUNG MUSS GENAUSO BLEIBEN
            allCsvOut=open(allCsvPath,"a")
            allCsvOut.write("%7d, %7d, %7d, %7d\n" % (tickNr,baumCnt,ascheCnt,feuerCnt))
            allCsvOut.close()
            print("\tStep:%7d\tTree:%7d\tAsh:%7d\tFire:%7d" % (tickNr,baumCnt,ascheCnt,feuerCnt))
            
        return newTick
    
    def animation(self,i):
        global tickNr
        fileName="waldbrand_run_"+berechnungslaufkennung+"/timesteps/step_"+berechnungslaufkennung+"_"+("%05d"%(tickNr))+".png"
        camera = Camera(self.simFig)
        
        self.treeDict =self.tick(self.treeDict)
        self.im.set_data(self.treeDict)
        
        plt.plot()
        camera.snap()
        anim = camera.animate()
        anim.save(fileName, writer='imagemagick')
        if tickNr >= (self.END_TICK&0xffffff): #hier das selbe wie mit <seed> am Anfang machen, um sicher zu gehen dass das Vorzeichenbit nicht gesetzt ist
            self.closeOp("Simulation finished")
            logdateiOut=open(Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log"),"a")
            logdateiOut.write("Time spent on job:\t"+str(time.time()-self.START_JOB)+"sec")
            sys.exit()      #hier muss das System per Instruktion herunter gefahren werden, 
                            #weil es nicht vom Eventhandler initiiert wurde
        tickNr+=1
if __name__=='__main__':
    if len(sys.argv)>1:
        #berechnungslaufkennung aus dem Verzeichnisnamen auslesen und dann kann das logfile geprueft werden
        berechnungslaufkennung = sys.argv[1][14:27]
        for line in open(Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log"),"r"):
            if "Simulation finished" in line:
                print("Simulation already finished")
#                exit()
        tickNr=0
    
    WaldbrandModell()
