#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Sven Reiter"
__version__ = "0.0.200419-210000"

# 
# Eine Berechnungslaufkennung wird generiert.
# Ein Verzeichnis fuer die Ergebnisse der aktuellen Simulation wird erstellt.
# Das Logfile wird erzeugt(so frueh wie moeglich um Fehler zu protokollieren).
# Die Unterverzeichnisse werden erstellt.
# Die Rohdateien der Simulationsergebnisse werden entspr. angelegt.
# Die CSV-Datei "waldbrand.inp" wird statisch eingelesen und die Werte entspr. initialisiert.
# Wenn CSV-Datei "waldbrand.inp" fehlt wird das Programm beendet.
# Wenn in der ertsen Zeile der CSV-Datei "waldbrand.inp" "Name,Value" fehlt => Abbruch; Fehler in Logfile protokollieren.
# Wenn Werte in der CSV-Datei fehlen werden sie im Logfile protokolliert.
# Wenn die CSV-Datei unerwartete Werte enthealt werden diese im Logfile protokolliert.
# Im Logfile wird auch angemerkt, wenn der Bestand der CSV-Datei konsistent war.
# Probleme bei der Verwendung des Filesystems werden im Logfile protokolliert.
# Eine Kopie der Eingabe-"waldbrand.inp" wird im Verzeichnis abgelegt.
# Die "ausgabedatei.out" wird entspr. beschrieben.
# Das System startet:
# Der random.seed wird aus "waldbrand.inp" uebernommen.
# Die Simulationsrelevanten Objekte und Strukturen werden deklariert/initialisiert.
#   Darunter auch <treeDict> was zweidimensional(<CELLWIDTH>x<CELLHEIGHT>) fuer jede Zelle mit 0 initialisiert wird und den Zustand jeder Zelle implizieren.
# Den erste Screenshot der noch leeren Flaeche wird nach /timesteps abgelegt.
# Bevor die mainloop betreten wird, wird geprueft ob START_TICK ungleich 0 ist
#   In dem Fall werden die Regeln so oft auf die Zellen angewendet bis <tickNr> == <START_TICK> und die grafishce Simulation begonnen werden kann
# Die mainloop wird gestartet in der das System bis zum Ende bleibt.
#   Zuerst wird geprueft tickNr schon END_TICK erreicht hat
#       im Falle dessen wird wie bei ESC auch das Programm beendet.
#   Danach wird pygames Eventqueue iteriert
#       Die Eventqueue wird iteriert um abzufangen ob das Programm beendet wurde.
#           Es werden Strukturen deklariert um die Anzahlen der Zellen des jeweiligen Zustands von jedem Simulations schritt festzuhalten.
#           In diesem Fall wird die CSV-Datei welche die ZellZustandAnzahlen der Simulationsschritte enthaelt iteriert und entspr. ZellZustandAnzahlen in entspr. Sequenz an entspr. Position(Timestep) geschrieben und infolge dessen die Diagramme geplottet.
#           Es werden alle Bilddateien aus dem /ticks extrahiert und einer Sequenz uebergeben.
#           Die Bilder der sortierten Sequenz werden zu einer .mp4 komprimiert und nach /movies geschrieben.
#   Wenn das Programm nicht beendet wurde geht es hier weiter
#   tick() wird mit <treeDict> aufgerufen
#       in <treeDict> stehen die ZellZustandBelegung des vergangenen Simulationsschritts.
#       <treeDict> wird iteriert und auf jede Zelle werden die Regeln angewendet und die Zellzustaende entspr. manipuliert.
#       zurueckgegeben wird das manipulierte <treeDict>
#       Erst nachdem <tickNr> == <START_TICK> werden die Messergebnisse persistent gespeichert.
#   <treeDict> wird iteriert und mit colourGrid() werden die Zellen entspr. ihres Zustands gefaerbt
#   abschliessend wird das Gitter ueber die Zellen gezeichnet, die Bildflaeche aktualisiert, das FPS-Intervall abgewartet, Anzahl der Bilder inkrementiert und ein Screenshot der gezeichneten Flaeche bzw. des aktuellen Simulationsschrittes nach /timesteps geschrieben.
           
import pygame, os, cv2, time, csv, errno
from pygame.locals import *
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from shutil import copyfile

class WaldbrandModell:
    timeSeed=0
    FPS = 10
    VERSION=""
    WINDOWWIDTH = 640
    WINDOWHEIGHT = 480
    CELLSIZE = 10    
    CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)     #Anz Zellen ueber die Breite
    CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)   #Anz Zellen ueber die Hoehe    
    SEITENVERHAELTNIS_B=4
    SEITENVERHAELTNIS_H=3    
    BLACK =    (0,  0,  0)
    WHITE =    (255,255,255)
    DARKGRAY = (40, 40, 40)
    GREEN =    (0,255,0)
    RED =      (255,0,0)    
    GAMMA = 0.00005                 #Wahrscheinlichkeit fuer einen Blitzeinschlag
    ALPHA = 0.01                    #Wahrscheinlichkeit dass aus Asche ein Baum waechst    
    ASCHE=0
    BAUM=1
    FEUER=2
    END_TICK=(-1)
    START_TICK=0

    def __init__(self):
        global eingabedatei,berechnungslaufkennung,eingabedateiPath,ausgabedateiPath,timestepsPath,graphsPath,moviesPath,ascheGraphPath,baumGraphPath,feuerGraphPath,allGraphPath,allCsvPath,allCsvOut
        
        eingabedatei="waldbrand.inp"
        if not Path(eingabedatei).exists():
            print("When opening "+eingabedatei+": No such directory entry\n")
            return;
            
        named_tuple = time.localtime()
        berechnungslaufkennung = time.strftime("%y%m%d-%H%M%S", named_tuple)
        
        #2. Verzeichnis "waldbrand_run_"+<berechnungslaufkennung> anlegen.
        try:
            os.mkdir("waldbrand_run_"+berechnungslaufkennung)
        except OSError as exc:
            if exc.errno==errno.EEXIST:
                print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
            elif exc.errno==errno.EACCES:
                print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
            elif exc.errno==errno.EROFS:
                print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
            elif exc.errno==errno.ENOENT:
               print("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
            exit(1)
        
        #6. log-Datei erzeugen
        logdateiPath=Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".log")
        logdateiOut=open(logdateiPath,"a")
        
        #4. weitere Ordnerstruktur arrangieren
        timestepsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/timesteps")
        graphsPath=Path("waldbrand_run_"+berechnungslaufkennung+"/graphs")
        moviesPath=Path("waldbrand_run_"+berechnungslaufkennung+"/movies")
        try:
            os.mkdir(timestepsPath)
            os.mkdir(graphsPath)
            os.mkdir(moviesPath)
        except OSError as exc:
            if exc.errno==errno.EEXIST:
                logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": File exists\n")
            elif exc.errno==errno.EACCES:
                logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
            elif exc.errno==errno.EROFS:
                logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Read-only file system\n")
            elif exc.errno==errno.ENOENT:
                logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": No such directory entry\n")
            exit(1)
            
        ascheGraphPath=Path(str(graphsPath)+"/asche_"+berechnungslaufkennung+".png")
        baumGraphPath=Path(str(graphsPath)+"/baum_"+berechnungslaufkennung+".png")
        feuerGraphPath=Path(str(graphsPath)+"/feuer_"+berechnungslaufkennung+".png")
        allGraphPath=Path(str(graphsPath)+"/all_"+berechnungslaufkennung+".png")
        allCsvPath=Path(str(graphsPath)+"/all_"+berechnungslaufkennung+".csv")
        try:
            allCsvOut=open(allCsvPath,"a")
        except FileNotFoundError:
            logdateiOut.write("When opening "+graphsPath+": No such directory entry\n")
        
        allCsvOut.write("Timestep,AnzBaum,AnzAsche,AnzFeuer\n")
            
        with open(eingabedatei) as f:
            firstline=f.readline()
            if "Name" not in firstline or "Value" not in firstline:
                logdateiOut.write("Die erste Zeile aus 'waldbrand.inp' deklariert nicht die Spalten 'Name' und 'Value'")
                return
        
        v=w=h=sb=sh=g=a=s=False
        with open(eingabedatei, "r") as csvdatei:
            csv_reader_object = csv.DictReader(csvdatei)
            for row in csv_reader_object:           # csv_reader_object wird iteriert als <row>
                for key, value in row.items():      # <row.items> wird separiert in key- und value- Teil
                    if key=="Name":
                        if   value=="VERSION":
#                            print(" In geladenem <VERSION> steht: "+row["Value"])  #TEST
                            self.VERSION=str(row["Value"])
                            v=self.VERSION!=""
                        elif value=="WIDTH":
#                            print(" In geladenem <WIDTH> steht: "+row["Value"])  #TEST
                            self.WINDOWWIDTH=int(row["Value"])
                            w=self.WINDOWWIDTH!=0
                        elif value=="HEIGHT":
#                            print(" In geladenem <HEIGHT> steht: "+row["Value"]) #TEST
                            self.WINDOWHEIGHT=int(row["Value"])
                            h=self.WINDOWHEIGHT!=0
                        elif value=="SEITENVERHAELTNIS_B":
#                            print(" In geladenem <SEITENVERHAELTNIS_B> steht: "+row["Value"])  #TEST
                            self.SEITENVERHAELTNIS_B=int(row["Value"])
                            sb=self.SEITENVERHAELTNIS_B!=0
                        elif value=="SEITENVERHAELTNIS_H":
#                            print(" In geladenem <SEITENVERHAELTNIS_H> steht: "+row["Value"])  #TEST
                            self.SEITENVERHAELTNIS_H=int(row["Value"])
                            sh=self.SEITENVERHAELTNIS_H!=0
                        elif value=="GAMMA":
                            self.GAMMA = float(row["Value"])
#                            print(" In geladenem <GAMMA> steht: "+row["Value"])  #TEST
                            g=self.GAMMA!=0.0
                        elif value=="ALPHA":
                            self.ALPHA = float(row["Value"])
#                            print(" In geladenem <ALPHA> steht: "+row["Value"])  #TEST
                            a=self.ALPHA!=0.0
                        elif value=="SEED":
                            self.timeSeed=int(row["Value"])
                            s=self.timeSeed!=0
                            self.timeSeed&=0xffffff
#                            print(" In geladenem <SEED> steht: "+row["Value"])   #TEST
                        elif value=="END_TICK":
                            self.END_TICK = int(row["Value"])
                        #   <END_TICK> muss nicht gesetzt sein um konsistenten Zustand zur Laufzeit zu gewährleisten
                        elif value=="START_TICK":
                            self.START_TICK = int(row["Value"])
                        #   <START_TICK> muss nicht gesetzt sein um konsistenten Zustand zur Laufzeit zu gewährleisten
                        else:
                            logdateiOut.write(eingabedatei+" enthält die redundante Information "+value+"\n")
                            
                    self.CELLWIDTH = int(self.WINDOWWIDTH / self.CELLSIZE)     #Anz Zellen ueber die Breite
                    self.CELLHEIGHT = int(self.WINDOWHEIGHT / self.CELLSIZE)   #Anz Zellen ueber die Hoehe
                    
        if v==False: logdateiOut.write(eingabedatei+" enthält keine Information zur entspr. Version\n")
        if w==False: logdateiOut.write(eingabedatei+" enthält keine Information über Anz. Pixel der Bildschirmbreite\n")
        if h==False: logdateiOut.write(eingabedatei+" enthält keine Information über Anz. Pixel der Bildschirmhöhe\n")
        if sb==False:logdateiOut.write(eingabedatei+" enthält keine Information über das Seitenverhältnis der Waagerechten\n")
        if sh==False:logdateiOut.write(eingabedatei+" enthält keine Information über das Seitenverhältnis der Senkerechten\n")
        if g==False: logdateiOut.write(eingabedatei+" enthält keine Information über den Wert der Wahrscheinlichkeit Gamma\n")
        if a==False: logdateiOut.write(eingabedatei+" enthält keine Information über den Wert der Wahrscheinlichkeit Alpha\n")
        if s==False: logdateiOut.write(eingabedatei+" enthält keine Information über den Seed\n")
        if v==w==h==sb==sh==g==a==s==v==True: logdateiOut.write("Die Informationen aus "+eingabedatei+" sind vollständig\n")
        
        try:    
            copyfile(eingabedatei,'waldbrand_run_'+berechnungslaufkennung+'/waldbrand_'+berechnungslaufkennung+".inp")
        except IOError as exc:     # zB. wenn die Berechtigung fehlt
            if exc.errno==errno.EACCES:
                logdateiOut.write("When creating subdirectories of /waldbrand_run_"+berechnungslaufkennung+": Permission denied\n")
            exit(1)
            
        eingabedaten=""
        berechnungsergebnisse=""
        tabellen=""
        
        ausgabedatei="Programm: "+eingabedatei+"\nProgrammversion: "+self.VERSION+"\nEingabedaten: "+eingabedaten+"\nBerechnungsergebnisse: "+berechnungsergebnisse+"\nTabellen: "+tabellen
        ausgabedateiPath=Path("waldbrand_run_"+berechnungslaufkennung+"/waldbrand_"+berechnungslaufkennung+".out")
        ausgabedateiOut=open(ausgabedateiPath,"w")                #=> anlegen und Referenz uebergeben
        ausgabedateiOut.write(ausgabedatei)
        
        self.setup()
        
    def setup(self):
        global tickNr
        
        if(self.timeSeed == 0):
            self.timeSeed = int(round(time.time()))
            self.timeSeed&=0xffffff #seed() fordert einen vorzeichenlosen 32bit Integer, deswegen die verUNDung 
                                    #so dass existierendes Muster erhalten bleibt und Vorzeichenbit 0 wird.
        np.random.seed(self.timeSeed)
        
        print("Machine started!")
        
        pygame.init() #initialize all imported pygame modules is a convenient way to get everything started.
        global DISPLAYSURF      # global defined surfaceobject, makes scope wide enough 
        FPSCLOCK = pygame.time.Clock()  #The Clock object’s tick() method should be called at the very end 
                                        #of the game loop, after the call to pygame.display.update(). 
        DISPLAYSURF = pygame.display.set_mode((self.WINDOWWIDTH,self.WINDOWHEIGHT))   #returns the pygame.Surface 
                                                                            #object for the window.
        pygame.display.set_caption('Waldbrandmodell')
        window = pygame.display.set_mode()      #Fläche zum aufnehmen deklarieren   PICTURE_CAPTURING
        
        # <treeDict> wird zuerst mit einem leeren Array der Groesse des abgebildeten Netzes initialisiert,
        #            zur Laufzeit wird mit dieser Datenstruktur der Zustand jeder Zelle gemerkt
        treeDict={}                         #leere Sequenz
        for y in range (self.CELLHEIGHT):        
            for x in range (self.CELLWIDTH):
                treeDict[x,y] = 0   #Form der Sequenz wird 2 dimensional definiert und 
                                    #jedes Element wird genullt
    
        for item in treeDict:   #je nach Information des/r Sets/Sequenz wird die grafische Zelle eingefaerbt
                self.colourGrid(item, treeDict)
                
        #zeichnung des Gitters, erst nachdem die Zellen gefaerbt wurden, weil ansonsten Gitter uebermalt
        for x in range(0, self.WINDOWWIDTH, self.CELLSIZE):
            pygame.draw.line(DISPLAYSURF, self.BLACK, (x,0),(x,self.WINDOWHEIGHT))
        for y in range (0, self.WINDOWHEIGHT, self.CELLSIZE):
            pygame.draw.line(DISPLAYSURF, self.BLACK, (0,y), (self.WINDOWWIDTH, y))
            
        pygame.display.update()                     # Die bemalte Oberflaeche wird aktualisiert
                
        tickNr=0                                                #BildId anlegen     PICTURE_CAPTURING
#        fileName="waldbrand_run_"+berechnungslaufkennung+"/timesteps/step_"+berechnungslaufkennung+"_"+("%05d"%(tickNr))+"TickCapture.jpg"  #Dateiname anlegen  PICTURE_CAPTURING
#        pygame.image.save(window,fileName)                      #Erstes Bild        PICTURE_CAPTURING
                                                                #aufnehmen:= Netz mit Asche
        if self.START_TICK!=0:
            while(tickNr!=self.START_TICK):
                treeDict = self.tick(treeDict)       #tick bzw. aktuallisierung jeder informativen Zelle
                tickNr+=1
            
        while True: #main loop
            if (-1 < self.END_TICK < tickNr):
                self.breakUp();
                return              #Um der Exception vorzubeugen
            
            for event in pygame.event.get():
                if event.type == pygame.locals.QUIT:
                    self.breakUp();
                    return              #Um der Exception vorzubeugen
            
            print("TickCapture"+("%5d"%(tickNr))+":")
            treeDict = self.tick(treeDict)       #tick bzw. aktuallisierung jeder informativen Zelle
            
            for item in treeDict:           #je nach Wert <item> wird die grafische Zelle eingefaerbt
                self.colourGrid(item, treeDict)
    
            #zeichnung des Gitters
            for x in range(0, self.WINDOWWIDTH, self.CELLSIZE):
                pygame.draw.line(DISPLAYSURF, self.BLACK, (x,0),(x,self.WINDOWHEIGHT))
            for y in range (0, self.WINDOWHEIGHT, self.CELLSIZE):
                pygame.draw.line(DISPLAYSURF, self.BLACK, (0,y), (self.WINDOWWIDTH, y))
            
            pygame.display.update()                 # Die bemalte Oberflaeche wird aktualisiert
            FPSCLOCK.tick(self.FPS)
            
            fileName="waldbrand_run_"+berechnungslaufkennung+"/timesteps/step_"+berechnungslaufkennung+"_"+("%05d"%(tickNr))+"TickCapture.jpg"   #Name aktualisieren PICTURE_CAPTURING
            pygame.image.save(window,fileName)                  #Bild aufnehmen     PICTURE_CAPTURING
            tickNr+=1                                       #BildId inkrementieren  PICTURE_CAPTURING 
        
    def breakUp(self):
        baums=[]
        asches=[]
        feuers=[]
                    
        with open("waldbrand_run_"+berechnungslaufkennung+"/graphs/all_"+berechnungslaufkennung+".csv", "r") as csvdatei:
            csv_reader_object = csv.DictReader(csvdatei)
            for row in csv_reader_object:           # csv_reader_object wird iteriert als <row>
                for key, value in row.items():      # <row.items> wird separiert in key- und value- Teil
                    if key=="AnzBaum":
                        baums.append(int(row["AnzBaum"]))
                    elif key=="AnzAsche":
                        asches.append(int(row["AnzAsche"]))
                    elif key=="AnzFeuer":
                        feuers.append(int(row["AnzFeuer"]))
                    
        figure =plt.figure()
        allPlot=figure.add_subplot(1,1,1)
        allPlot.set_xlabel('Simulationsfortschritt[Bild]')
        allPlot.set_ylabel('Anzahl Zellen(Rot:=Feuer,Grün:=Baum,Grau:=Asche)')
        allPlot.plot(baums,color='green')
        allPlot.plot(asches,color='darkgray')
        allPlot.plot(feuers,color='red')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/all_'+berechnungslaufkennung+'.png')
        
        figure =plt.figure()
        baumPlot=figure.add_subplot(1,1,1)
        baumPlot.set_xlabel('Simulationsfortschritt[Bild]')
        baumPlot.set_ylabel('Anzahl Baum-Zellen')
        baumPlot.plot(baums,color='green')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/baum_'+berechnungslaufkennung+'.png')
        
        figure =plt.figure()
        aschePlot=figure.add_subplot(1,1,1)
        aschePlot.set_xlabel('Simulationsfortschritt[Bild]')
        aschePlot.set_ylabel('Anzahl Asche-Zellen')
        aschePlot.plot(asches,color='darkgray')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/asche_'+berechnungslaufkennung+'.png')
        
        figure =plt.figure()
        feuerPlot=figure.add_subplot(1,1,1)
        feuerPlot.set_xlabel('Simulationsfortschritt[Bild]')
        feuerPlot.set_ylabel('Anzahl Feuer-Zellen')
        feuerPlot.plot(feuers,color='red')
        figure.savefig("waldbrand_run_"+berechnungslaufkennung+'/graphs/feuer_'+berechnungslaufkennung+'.png')
                
        plt.close
        
        imageFolder="waldbrand_run_"+berechnungslaufkennung+'/timesteps/'
        videoFile = "waldbrand_run_"+berechnungslaufkennung+'/movies/steps_'+berechnungslaufkennung+'_'+str("%05d"%(self.START_TICK))+'-'+str("%05d"%(tickNr-1)+'.mp4')
        imageSize = (self.WINDOWWIDTH,self.WINDOWHEIGHT)
        
        images = [img for img in os.listdir(imageFolder) if img.endswith(".jpg")]
        images.sort()   #Ansonsten ist die korrekte Reihenfolge der Einzelaufnahmen 
                        #nicht mehr gewährleistet.
                        #Einzelbilder der Videoausgabe liefen trotzdem vereinzelt in 
                        #falscher Reihenfolge ab, weil der Dateiname der Bilder 
                        #unguenstig gewaehlt wurde.
        out = cv2.VideoWriter(videoFile,cv2.VideoWriter_fourcc(*'mp4v'), self.FPS, imageSize)        
        imgArray=[]
        for filename in images:
            img = cv2.imread(os.path.join(imageFolder,filename))
            imgArray.append(img)
            out.write(img)
        out.release()
                    
        pygame.quit()
                
    #Farbt die grafischen Zellen, je nach Zustand der informativen Zelle der Sequenz <treeDict>
    def colourGrid(self, item, treeDict):
        x = item[0]
        y = item[1]
        y = y * self.CELLSIZE # Abbildung: <informative Zelle> -> <grafische Zelle> [Hoehe]
        x = x * self.CELLSIZE # Abbildung: <informative Zelle> -> <grafische Zelle> [Breite]
        if treeDict[item] == 0:
            pygame.draw.rect(DISPLAYSURF, self.DARKGRAY, (x, y, self.CELLSIZE, self.CELLSIZE))
        if treeDict[item] == 1:
            pygame.draw.rect(DISPLAYSURF, self.GREEN, (x, y, self.CELLSIZE, self.CELLSIZE))
        if treeDict[item] == 2:
            pygame.draw.rect(DISPLAYSURF, self.RED, (x, y, self.CELLSIZE, self.CELLSIZE))
        return None
    
    def isNeigborhoodBurning(self,item,treeDict):
        for x in range (-1,2):
            for y in range (-1,2):
                checkCell = (item[0]+x,item[1]+y)
                if checkCell[0] < self.CELLWIDTH  and checkCell[0] >=0:
                    if checkCell[1] < self.CELLHEIGHT and checkCell[1]>= 0:
                        if treeDict[checkCell] == self.FEUER:
                            return True
        return False
    
    #Bei jedem tick werden fuer jedes Element die drei Regeln befolgt
    # und entspr. der Regeln wird ein neues Set beschrieben und zurueckgegeben
    def tick(self,treeDict):
        global allCsvOut
        baumCnt=0
        ascheCnt=0
        feuerCnt=0
        newTick = {}                #zuerst wird neues Set erschaffen
        for item in treeDict:       #foreach <item> in <treeDict>
            if treeDict[item] == self.BAUM:
                if self.isNeigborhoodBurning(item,treeDict):
                    newTick[item] = self.FEUER
                else:
                    if np.random.uniform(0.,1.) < self.GAMMA:
                        newTick[item] = self.FEUER
                    else:
                        newTick[item] = self.BAUM
                        
                baumCnt=baumCnt+1
            elif treeDict[item] == self.ASCHE:
                if np.random.uniform(0.,1.) < self.ALPHA:
                    newTick[item] = self.BAUM
                else:
                    newTick[item] = self.ASCHE
                ascheCnt=ascheCnt+1
            elif treeDict[item] == self.FEUER:
                newTick[item] = self.ASCHE
                feuerCnt=feuerCnt+1
        
        if tickNr>=self.START_TICK:
            allCsvOut=open(allCsvPath,"a")
            allCsvOut.write("%5d, %5d, %5d, %5d\n" % (tickNr,baumCnt,ascheCnt,feuerCnt))
            allCsvOut.close()
            print("\tBaum:%5d,\tAsche:%5d,\tFeuer:%5d" % (baumCnt,ascheCnt,feuerCnt))
        return newTick

if __name__=='__main__':
    WaldbrandModell()